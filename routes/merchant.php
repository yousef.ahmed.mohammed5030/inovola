<?php

use App\Http\Controllers\Merchant\Api\Auth\CustomerAuthController;
use App\Http\Controllers\Merchant\Api\Auth\MerchantAuthController;
use App\Http\Controllers\Merchant\Api\Customer\CartController;
use App\Http\Controllers\Merchant\Api\Dashboard\ProductController;
use App\Http\Controllers\Merchant\Api\Customer\ProductController as CustomerProductController;
use App\Http\Controllers\Merchant\Api\Dashboard\UpdateInformationController;
use Illuminate\Support\Facades\Route;

Route::post('customers/login', [CustomerAuthController::class, 'login']);
Route::post('customers/register', [CustomerAuthController::class, 'register']);

Route::post('merchants/login', [MerchantAuthController::class, 'login']);


Route::middleware(['auth:merchant', 'belongToCurrentMerchant'])->prefix('dashboard')->group(function () {
    Route::put('update', UpdateInformationController::class);
    Route::apiResource('products', ProductController::class);
});

Route::get('products', [CustomerProductController::class, 'index']);

Route::middleware(['auth:customer', 'belongToCurrentMerchant'])->group(function () {
    Route::get('cart', [CartController::class, 'index']);
    Route::post('products/{product}/addToCart', [CartController::class, 'addToCart']);
});
