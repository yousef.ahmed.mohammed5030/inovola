# Inovola Task 


## Notes
There are two solutions to this types of project  :

1. one database for all merchants. (By adding merchant_id column in tables that related to merchant data)
2. single database for each merchant.

so for make task simple I preferred to make all merchants share same database but in big projects we must seperate them.

also i considered there are one user for merchant so i left merchant login data on merchants table 
but if there are more than one user manage merchant data we need to create new table like merchant_users or admins and mark them by merchant_id column .
## Every merchant has its subdomain

```
when merchant register he must choose it's subdomian name

example : 
    
    merchant1.example.com
```

## Installation

1) Download repo 


2) Install packages by running this command in your terminal/cmd:
```
composer install
```

3) copy .env.example to .env file :
```
cp .env.example .env
```
   
4) set database credentials
   

5) run following commands :
```
php artisan key:generate
php artisan migrate
```

6) run project :
```
php artisan serve
```


## notes
There are postman collection on project files.
