<?php

namespace App\Http\Middleware;

use App\Models\Merchant;
use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class MerchantDomain
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse|JsonResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $merchant = Merchant::where('domain', $request['merchantDomain'])->first();

        if (!$merchant) {
            return response()->json(['message' => 'invalid merchant'], 404);
        }

        app()->singleton('currentMerchant', function () use ($merchant) {
            return $merchant;
        });

        return $next($request);
    }
}
