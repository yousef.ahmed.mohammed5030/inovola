<?php

namespace App\Http\Middleware;

use App\Models\Customer;
use App\Models\Merchant;
use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class BelongToCurrentMerchant
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse|JsonResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if (auth()->user() instanceof Customer and auth()->user()['merchant_id'] != currentMerchant()['id']) {
            return response()->json(['message' => 'unauthorized'], 403);
        }

        if (auth()->user() instanceof Merchant and auth()->user()['id'] != currentMerchant()['id']) {
            return response()->json(['message' => 'unauthorized'], 403);
        }

        return $next($request);
    }
}
