<?php

namespace App\Http\Requests\Api\Auth;

use App\Rules\Subdomain;
use Illuminate\Foundation\Http\FormRequest;

class MerchantRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:190',
            'store_name' => 'required|string|max:190',
            'email' => 'required|email|max:190|unique:merchants',
            'password' => 'required|string|min:6|max:20',
            'domain' => [
                'required',
                'string',
                new Subdomain,
                'unique:merchants'
            ],
        ];
    }
}
