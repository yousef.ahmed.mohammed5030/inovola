<?php

namespace App\Http\Requests\Merchant\Api;

use Astrotomic\Translatable\Validation\RuleFactory;
use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return RuleFactory::make([
            '%name%' => 'required|string|max:190',
            '%description%' => 'required|string|max:1200',
            'price' => 'required|numeric',
            'price_vat_included' => 'required|in:0,1',
            'vat_percentage' => 'required_if:price_vat_included,0|numeric'
        ]);
    }
}
