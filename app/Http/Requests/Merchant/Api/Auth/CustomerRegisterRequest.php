<?php

namespace App\Http\Requests\Merchant\Api\Auth;

use App\Rules\Subdomain;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CustomerRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:190',
            'email' => [
                'required',
                'email',
                'max:190',
                Rule::unique('customers')->where('merchant_id', currentMerchant()->id)
            ],
            'password' => 'required|string|min:6|max:20',
        ];
    }
}
