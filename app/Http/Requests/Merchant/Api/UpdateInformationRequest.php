<?php

namespace App\Http\Requests\Merchant\Api;

use Astrotomic\Translatable\Validation\RuleFactory;
use Illuminate\Foundation\Http\FormRequest;

class UpdateInformationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return RuleFactory::make([
            'name' => 'required|string|max:190',
            'store_name' => 'required|string|max:190',
            'shipping_cost' => 'required|numeric',
        ]);
    }
}
