<?php

namespace App\Http\Controllers\Merchant\Api\Customer;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProductResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * Class ProductController
 * @package App\Http\Controllers\Merchant\Api\Customer
 */
class ProductController extends Controller
{
    /**
     * @return AnonymousResourceCollection
     */
    public function index(): AnonymousResourceCollection
    {
        return ProductResource::collection(currentMerchant()->products()->paginate());
    }
}
