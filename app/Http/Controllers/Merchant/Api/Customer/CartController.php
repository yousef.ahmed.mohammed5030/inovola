<?php

namespace App\Http\Controllers\Merchant\Api\Customer;

use App\Exceptions\NotBelongToCurrentMerchant;
use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\JsonResponse;

/**
 * Class CartController
 * @package App\Http\Controllers\Merchant\Api\Customer
 */
class CartController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return response()->json(cart()->toArray());
    }

    /**
     * @param $merchantDomain
     * @param Product $product
     *
     * @return JsonResponse
     * @throws NotBelongToCurrentMerchant
     */
    public function addToCart($merchantDomain, Product $product): JsonResponse
    {
        $product->belongToCurrentMerchant();

        return response()->json(cart()->add($product, request('quantity') ?? 1));
    }
}
