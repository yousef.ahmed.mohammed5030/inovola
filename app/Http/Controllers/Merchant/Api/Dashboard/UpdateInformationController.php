<?php

namespace App\Http\Controllers\Merchant\Api\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Merchant\Api\UpdateInformationRequest;
use App\Http\Resources\MerchantResource;

/**
 * Class UpdateInformationController
 * @package App\Http\Controllers\Merchant\Api\Dashboard
 */
class UpdateInformationController extends Controller
{
    /**
     * @param UpdateInformationRequest $request
     * @return MerchantResource
     */
    public function __invoke(UpdateInformationRequest $request): MerchantResource
    {
        currentMerchant()->update($request->validated());

        return new MerchantResource(currentMerchant());
    }
}
