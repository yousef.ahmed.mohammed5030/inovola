<?php

namespace App\Http\Controllers\Merchant\Api\Dashboard;

use App\Exceptions\NotBelongToCurrentMerchant;
use App\Http\Controllers\Controller;
use App\Http\Requests\Merchant\Api\ProductRequest;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * Class ProductController
 * @package App\Http\Controllers\Merchant\Api\Dashboard
 */
class ProductController extends Controller
{
    /**
     * @return AnonymousResourceCollection
     */
    public function index(): AnonymousResourceCollection
    {
        return ProductResource::collection(currentMerchant()->products()->paginate());
    }

    /**
     * @param ProductRequest $request
     * @return ProductResource
     */
    public function store(ProductRequest $request): ProductResource
    {
        return new ProductResource(currentMerchant()->products()->create($request->validated()));
    }

    /**
     * @param ProductRequest $request
     * @param $merchantDomain
     * @param Product $product
     *
     * @return ProductResource
     * @throws NotBelongToCurrentMerchant
     */
    public function update(ProductRequest $request, $merchantDomain, Product $product): ProductResource
    {
        $product->belongToCurrentMerchant();

        $product->update($request->validated());

        return new ProductResource($product);
    }

    /**
     * @param $merchantDomain
     * @param Product $product
     *
     * @return JsonResponse
     * @throws NotBelongToCurrentMerchant
     */
    public function destroy($merchantDomain, Product $product): JsonResponse
    {
        $product->belongToCurrentMerchant();

        $product->delete();

        return response()->json(['message' => 'Product deleted successfully']);
    }
}
