<?php

namespace App\Http\Controllers\Merchant\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Merchant\Api\Auth\MerchantLoginRequest;
use App\Http\Resources\MerchantResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;

/**
 * Class MerchantAuthController
 * @package App\Http\Controllers\Merchant\Api\Auth
 */
class MerchantAuthController extends Controller
{
    /**
     * @param MerchantLoginRequest $request
     *
     * @return MerchantResource|JsonResponse
     */
    public function login(MerchantLoginRequest $request)
    {
        if (currentMerchant()['email'] != $request['email'] or !Hash::check($request['password'], currentMerchant()['password'])) {
            return response()->json(['message' => 'unauthenticated'], 401);
        }

        return (new MerchantResource(currentMerchant()))->additional(
            ['access_token' => currentMerchant()->createToken('token')->plainTextToken]
        );
    }
}
