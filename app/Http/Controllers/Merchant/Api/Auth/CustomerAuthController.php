<?php

namespace App\Http\Controllers\Merchant\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Merchant\Api\Auth\CustomerLoginRequest;
use App\Http\Requests\Merchant\Api\Auth\CustomerRegisterRequest;
use App\Http\Resources\CustomerResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;

/**
 * Class CustomerAuthController
 * @package App\Http\Controllers\Merchant\Api\Auth
 */
class CustomerAuthController extends Controller
{
    /**
     * @param CustomerLoginRequest $request
     *
     * @return CustomerResource|JsonResponse
     */
    public function login(CustomerLoginRequest $request)
    {
        $customer = currentMerchant()->getCustomer($request['email']);

        if (!$customer or !Hash::check($request['password'], $customer->password)) {
            return response()->json(['message' => 'unauthenticated'], 401);
        }

        return (new CustomerResource($customer))->additional(
            ['access_token' => $customer->createToken('token')->plainTextToken]
        );
    }

    /**
     * @param CustomerRegisterRequest $request
     *
     * @return CustomerResource
     */
    public function register(CustomerRegisterRequest $request): CustomerResource
    {
        return new CustomerResource(currentMerchant()->customers()->create($request->validated()));
    }
}
