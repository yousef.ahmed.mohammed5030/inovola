<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Auth\MerchantRegisterRequest;
use App\Http\Resources\MerchantResource;
use App\Models\Merchant;

/**
 * Class MerchantAuthController
 * @package App\Http\Controllers\Api
 */
class MerchantAuthController extends Controller
{
    /**
     * @param MerchantRegisterRequest $request
     * @return MerchantResource
     */
    public function __invoke(MerchantRegisterRequest $request): MerchantResource
    {
        return new MerchantResource(Merchant::create($request->validated()));
    }
}
