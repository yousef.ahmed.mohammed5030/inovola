<?php

namespace App\Models\Helpers;

use App\Exceptions\NotBelongToCurrentMerchant;

/**
 * Trait GlobalHelper
 * @package App\Models\Helpers
 */
trait GlobalHelper
{
    /**
     * @return bool
     * @throws NotBelongToCurrentMerchant
     */
    public function belongToCurrentMerchant(): bool
    {
        if ($this['merchant_id'] != currentMerchant()->id) {
            throw new NotBelongToCurrentMerchant;
        }

        return true;
    }
}
