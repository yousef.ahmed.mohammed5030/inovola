<?php

namespace App\Models\Helpers;

/**
 * Trait ProductHelper
 * @package App\Models\Helpers
 */
trait ProductHelper
{
    /**
     * @param $value
     */
    public function setVatPercentageAttribute($value)
    {
        if (!$this->attributes['price_vat_included']) {
            $this->attributes['vat_percentage'] = $value;
        } else {
            $this->attributes['vat_percentage'] = 0;
        }
    }
}
