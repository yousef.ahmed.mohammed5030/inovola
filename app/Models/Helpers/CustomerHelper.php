<?php

namespace App\Models\Helpers;

/**
 * Trait CustomerHelper
 * @package App\Models\Helpers
 */
trait CustomerHelper
{
    /**
     * @param $password
     */
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }
}
