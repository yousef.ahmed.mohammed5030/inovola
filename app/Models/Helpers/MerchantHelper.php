<?php

namespace App\Models\Helpers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Trait MerchantHelper
 * @package App\Models\Helpers
 */
trait MerchantHelper
{
    /**
     * @param $password
     */
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

    /**
     * @param $email
     * @return Model|HasMany|object|null
     */
    public function getCustomer($email)
    {
        return $this->customers()->where('email', '=', $email)->first();
    }
}
