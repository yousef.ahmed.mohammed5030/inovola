<?php

namespace App\Models;

use App\Models\Relations\CartRelation;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Cart
 * @package App\Models
 */
class Cart extends Model
{
    use CartRelation;

    /**
     * @var string[]
     */
    protected $fillable = [
        'customer_id',
    ];
}
