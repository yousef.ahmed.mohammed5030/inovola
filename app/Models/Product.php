<?php

namespace App\Models;

use App\Models\Helpers\GlobalHelper;
use App\Models\Helpers\ProductHelper;
use App\Models\Translations\ProductTranslation;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Product extends Model implements TranslatableContract
{
    use HasFactory, Translatable, ProductHelper, GlobalHelper;

    /**
     * @var string[]
     */
    public $translatedAttributes = [
        'name',
        'description'
    ];

    /**
     * @var string[]
     */
    protected $fillable = [
        'price',
        'price_vat_included',
        'vat_percentage'
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'price' => 'double',
        'price_vat_included' => 'int',
        'vat_percentage' => 'double'
    ];

    /**
     * @var string
     */
    protected $translationModel = ProductTranslation::class;
}
