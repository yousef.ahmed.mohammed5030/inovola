<?php

namespace App\Models\Relations;

use App\Models\Product;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Trait CartItemRelation
 * @package App\Models\Relations
 */
trait CartItemRelation
{
    /**
     * @return BelongsTo
     */
    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }
}
