<?php

namespace App\Models\Relations;

use App\Models\Customer;
use App\Models\Product;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Trait MerchantRelation
 * @package App\Models\Relations
 */
trait MerchantRelation
{
    /**
     * @return HasMany
     */
    public function customers(): HasMany
    {
        return $this->hasMany(Customer::class);
    }

    /**
     * @return HasMany
     */
    public function products(): HasMany
    {
        return $this->hasMany(Product::class);
    }
}
