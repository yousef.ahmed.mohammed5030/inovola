<?php

namespace App\Models\Relations;

use App\Models\CartItem;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Trait CartRelation
 * @package App\Models\Relations
 */
trait CartRelation
{
    /**
     * @return HasMany
     */
    public function items(): HasMany
    {
        return $this->hasMany(CartItem::class);
    }
}
