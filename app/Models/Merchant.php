<?php

namespace App\Models;

use App\Models\Helpers\MerchantHelper;
use App\Models\Relations\MerchantRelation;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Sanctum\HasApiTokens;

class Merchant extends Model
{
    use HasFactory, MerchantHelper, MerchantRelation,HasApiTokens;

    /**
     * @var string[]
     */
    protected $fillable = [
        'name',
        'store_name',
        'domain',
        'email',
        'password',
        'shipping_cost'
    ];

    protected $casts = [
        'shipping_cost' => 'double'
    ];
}
