<?php

namespace App\Models;

use App\Models\Relations\CartItemRelation;
use Illuminate\Database\Eloquent\Model;

/**
 * Class CartItem
 * @package App\Models
 */
class CartItem extends Model
{
    use CartItemRelation;

    /**
     * @var string[]
     */
    protected $fillable = [
        'cart_id',
        'product_id',
        'quantity'
    ];
}
