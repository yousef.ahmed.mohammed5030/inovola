<?php

use App\Models\Merchant;
use App\Services\Cart\Cart;

if (!function_exists('currentMerchant')) {
    /**
     * Resolve current merchant from the container.
     *
     * @return Merchant
     */
    function currentMerchant(): Merchant
    {
        return resolve('currentMerchant');
    }
}


if (!function_exists('cart')) {
    /**
     * @return Cart
     */
    function cart(): Cart
    {
        static $cart;

        if ($cart) {
            return $cart;
        }

        return $cart = app(Cart::class);
    }
}
