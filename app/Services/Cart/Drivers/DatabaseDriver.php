<?php

namespace App\Services\Cart\Drivers;

use App\Models\Cart;
use App\Models\CartItem;

class DatabaseDriver
{
    /**
     * Returns current cart data.
     *
     * @return array
     */
    public function getCartData(): array
    {
        $cartData = Cart::with([
            'items' => function ($query) {
                $query
                    ->join('products', 'products.id', 'cart_items.product_id')
                    ->join('product_translations', 'product_translations.product_id', 'products.id')
                    ->where('product_translations.locale', app()->getLocale())
                    ->select([
                        'cart_items.cart_id',
                        'cart_items.id',
                        'cart_items.quantity',
                        'cart_items.product_id',
                        'product_translations.name',
                        'products.price',
                        'products.price_vat_included',
                        'products.vat_percentage'
                    ]);
            },
        ])->where('customer_id', '=', auth()->user()->id)->first();

        if (!$cartData) {
            return [];
        }

        return $cartData->toArray();
    }

    /**
     * Add a new cart item to the database.
     *
     * @param int Cart id
     * @param array Cart item data
     * @return void
     */
    public function addCartItem($cartId, $cartItem)
    {
        CartItem::create([
            'cart_id' => $cartId,
            'product_id' => $cartItem['product_id'],
            'quantity' => $cartItem['quantity']
        ]);
    }

    /**
     * Stores the cart and cart items data in the database tables.
     *
     * @param array Cart data
     * @return void
     */
    public function storeNewCartData($cartData)
    {
        $cartItems = $cartData['items'];

        $cartId = $this->storeCartDetails();

        foreach ($cartItems as $cartItem) {
            $this->addCartItem($cartId, $cartItem);
        }
    }

    /**
     * Stores the cart data in the database table and returns the id of the record.
     *
     * @return int
     */
    protected function storeCartDetails(): int
    {
        $cart = Cart::updateOrCreate(['customer_id' => auth()->user()->id]);

        return $cart->id;
    }

    /**
     * Updates the quantity in the cart items table.
     *
     * @param int Id of the cart item
     * @param int New quantity to be set
     * @return void
     */
    public function setCartItemQuantity($cartItemId, $newQuantity)
    {
        CartItem::where('id', $cartItemId)->update(['quantity' => $newQuantity]);
    }
}
