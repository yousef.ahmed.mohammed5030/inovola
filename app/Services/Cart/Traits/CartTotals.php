<?php

namespace App\Services\Cart\Traits;

/**
 * Trait CartTotals
 * @package App\Services\Cart\Traits
 */
trait CartTotals
{
    /**
     * Sets the total variables of the object.
     *
     * @return void
     */
    protected function updateTotals()
    {
        $this->clear();

        $this->setSubtotal();

        $this->setShippingCost();

        $this->total = round($this->subtotal + $this->shippingCost + $this->tax, 2);
    }

    /**
     * Sets the subtotal of the cart.
     *
     * @return void
     */
    protected function setSubtotal()
    {
        foreach ($this->items as $item) {
            $this->subtotal += $item->quantity * $item->price;
            $this->tax += $item->quantity * $item->tax;
        }
    }

    /**
     * Sets the shipping charges of the cart.
     *
     * @return void
     */
    protected function setShippingCost()
    {
        $this->shippingCost = currentMerchant()->shipping_cost;
    }

    protected function clear()
    {
        $this->subtotal = 0;
        $this->total = 0;
        $this->tax = 0;
        $this->shippingCost = 0;
    }
}
