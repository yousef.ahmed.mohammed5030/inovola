<?php

namespace App\Services\Cart\Traits;

use App\Models\Product;
use App\Services\Cart\CartItem;

/**
 * Trait CartItemsManager
 * @package App\Services\Cart\Traits
 */
trait CartItemsManager
{
    /**
     * Adds an item to the cart.
     *
     * @param Product $product
     * @param int Quantity
     * @return array
     * @throws ItemMissing
     */
    public function add($product, $quantity)
    {
        if ($this->itemExists($product)) {
            $cartItemIndex = $this->items->search($this->cartItemsCheck($product));

            return $this->incrementQuantityAt($cartItemIndex, $quantity);
        }

        $this->items->push(CartItem::createFrom($product, $quantity));

        return $this->cartUpdates($isNewItem = true);
    }

    /**
     * Checks if an item already exists in the cart.
     *
     * @param Product $product
     * @return bool
     */
    public function itemExists($product): bool
    {
        return $this->items->contains($this->cartItemsCheck($product));
    }

    /**
     * Checks if a cart item with the specified entity already exists.
     *
     * @param Product $product
     * @return \Closure
     */
    protected function cartItemsCheck($product): \Closure
    {
        return function ($item) use ($product) {
            return $item->productId == $product->id;
        };
    }

    /**
     * Increments the quantity of a cart item.
     *
     * @param int Index of the cart item
     * @param int quantity to be increased
     * @return array
     */
    public function incrementQuantityAt($cartItemIndex, $quantity = 1)
    {
        $this->items[$cartItemIndex]->quantity += $quantity;

        $this->cartDriver->setCartItemQuantity(
            $this->items[$cartItemIndex]->id,
            $this->items[$cartItemIndex]->quantity
        );

        return $this->cartUpdates();
    }
}
