<?php

namespace App\Services\Cart;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CartItem
 * @package App\Services\Cart
 */
class CartItem
{
    public $id = null;

    public $productId = null;

    public $name;

    public $price;

    public $tax;

    public $quantity = 1;

    /**
     * Creates a new cart item.
     *
     * @param Model|array
     * @param int Quantity of the item
     * @return CartItem
     */
    public function __construct($data, $quantity)
    {
        if (is_array($data)) {
            return $this->createFromArray($data);
        }

        return $this->createFromModel($data, $quantity);
    }

    /**
     * Creates a new cart item from a model instance.
     *
     * @param $product
     * @param int Quantity of the item
     * @return CartItem
     */
    protected function createFromModel($product, $quantity): CartItem
    {
        $this->productId = $product->id;
        $this->name = $product->name;
        $this->price = $product->price;
        $this->tax = $this->setTax($product);
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Creates a new cart item from an array.
     *
     * @param array
     * @return CartItem
     */
    protected function createFromArray($item): CartItem
    {
        $this->id = $item['id'];
        $this->productId = $item['product_id'];
        $this->name = $item['name'];
        $this->price = $item['price'];
        $this->tax = $this->setTax($item);
        $this->quantity = $item['quantity'];

        return $this;
    }

    /**
     * @param $product
     * @return double|int
     */
    private function setTax($product)
    {
        if ($product['price_vat_included']) {
            return 0;
        }

        return $product['price'] * ($product['vat_percentage'] / 100);
    }

    /**
     * Creates a new cart item from an entity.
     *
     * @param Model|array
     * @param int Quantity of the item
     * @return CartItem
     */
    public static function createFrom($data, $quantity = 1)
    {
        return new static($data, $quantity);
    }

    /**
     * Returns object properties as array.
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'product_id' => $this->productId,
            'name' => $this->name,
            'price' => $this->price,
            'quantity' => $this->quantity,
        ];
    }
}
