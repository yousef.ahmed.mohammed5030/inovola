<?php

namespace App\Services\Cart;

use App\Services\Cart\Drivers\DatabaseDriver;
use App\Services\Cart\Traits\CartItemsManager;
use App\Services\Cart\Traits\CartTotals;
use Illuminate\Contracts\Support\Arrayable;

/**
 * Class Cart
 * @package App\Services\Cart
 */
class Cart implements Arrayable
{
    use CartItemsManager, CartTotals;

    /**
     * @var DatabaseDriver
     */
    protected $cartDriver;

    /**
     * @var null
     */
    protected $id = null;

    /**
     * @var array
     */
    protected $items = [];

    /**
     * @var int
     */
    protected $subtotal = 0;

    /**
     * @var int
     */
    protected $shippingCost = 0;

    /**
     * @var int
     */
    protected $tax = 0;

    /**
     * @var int
     */
    protected $total = 0;

    /**
     * Cart constructor.
     * @param DatabaseDriver $driver
     */
    public function __construct(DatabaseDriver $driver)
    {
        $this->cartDriver = $driver;

        $this->items = collect($this->items);

        if ($cartData = $this->cartDriver->getCartData()) {
            $this->id = $cartData['id'];

            $this->setItems($cartData['items']);

            $this->updateTotals();
        }
    }

    public function setItems($cartItems)
    {
        foreach ($cartItems as $cartItem) {
            $this->items->push(CartItem::createFrom($cartItem));
        }
    }

    /**
     * Performs cart updates and returns the data.
     *
     * @param bool Weather its a new item or existing
     * @return array
     */
    protected function cartUpdates($isNewItem = false): array
    {
        $this->updateTotals();

        $this->storeCartData($isNewItem);

        return $this->toArray();
    }

    /**
     * Stores the cart data on the cart driver.
     *
     * @param bool Weather its a new item or an existing one
     * @return void
     */
    protected function storeCartData($isNewItem = false)
    {
        if ($this->id) {

            if ($isNewItem) {
                $this->cartDriver->addCartItem($this->id, $this->items->last()->toArray());
            }

            return;
        }

        $this->cartDriver->storeNewCartData($this->toArray());
    }

    public function toArray()
    {
        return [
            'subtotal' => (double)round($this->subtotal, 2),
            'tax' => (double)round($this->tax, 2),
            'shipping_cost' => (double)round($this->shippingCost, 2),
            'total' => (double)$this->total,
            'items' => $this->items->map->toArray()
        ];
    }
}
