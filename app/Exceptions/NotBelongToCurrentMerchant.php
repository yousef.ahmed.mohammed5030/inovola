<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\Request;

class NotBelongToCurrentMerchant extends Exception
{
    /**
     * Render the exception into an HTTP response.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function render($request)
    {
        return response()->json([
            'message' => 'Model not belong to current merchant'
        ], 403);
    }
}
