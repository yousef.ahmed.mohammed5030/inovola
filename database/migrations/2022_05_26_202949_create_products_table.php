<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();

            $table->double('price',10,2);

            $table->tinyInteger('price_vat_included')->default(0);
            $table->double('vat_percentage',5,2)->default(0);

            $table->unsignedBigInteger('merchant_id');
            $table->foreign('merchant_id')
                ->on('merchants')
                ->references('id')
                ->onDelete('cascade');

            $table->timestamps();
        });

        Schema::create('product_translations', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('product_id');
            $table->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onDelete('cascade');

            $table->string('locale')->index();
            $table->string('name');
            $table->longText('description');

            $table->unique(['product_id', 'locale']);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_translations');
        Schema::dropIfExists('products');
    }
}
