<?php

namespace Database\Seeders;

use App\Models\Merchant;
use Illuminate\Database\Seeder;

class MerchantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Merchant::factory()->create([
            'email' => 'merchant1@gmail.com',
            'domain' => 'merchant1'
        ]);

        Merchant::factory()->create([
            'email' => 'merchant2@gmail.com',
            'domain' => 'merchant2'
        ]);
    }
}
